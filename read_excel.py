from transliterate import to_latin, to_cyrillic


def translit_workbook(wb):
    for ws_index, ws in enumerate(wb.worksheets):
        print(f"===Working on sheet#{ws_index+1}===")
        for row in ws.iter_rows():
            for cell in row:
                if cell.value and cell.data_type == 's':
                    ws[cell.coordinate] = to_latin(cell.value)
    return wb

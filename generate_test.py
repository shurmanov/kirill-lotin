from openpyxl import load_workbook, Workbook
from transliterate import to_latin, to_cyrillic


def read_ex(wb):
    for ws_index, ws in enumerate(wb.worksheets):
        print(f"===Working on sheet#{ws_index+1}===")
        for row in ws.iter_rows():
            for cell in row:
                if cell.value and cell.data_type == 's':
                    ws[cell.coordinate] = to_latin(cell.value)
    return wb


if __name__ == '__main__':
    wb = load_workbook('/Users/shurmanov/Desktop/fix-T4-tay.xlsx')

    vars = [[],[],[],[],[]]

    for i in range(100):
        vars[i%5].append(i)

    for ws_index, ws in enumerate(wb.worksheets):
        print(f"===Working on sheet#{ws_index+1}===")
        for i, var in enumerate(vars):
            for j, r in enumerate(var):
                ws.cell(i*20+1+j, 7+1).value = ws.cell(1+r, 1+1).value
                ws.cell(i*20+1+j, 7+2).value = f"a- {ws.cell(1+r, 1+2).value}"
                ws.cell(i*20+1+j, 7+3).value = f"b - {ws.cell(1+r, 1+3).value}"
                ws.cell(i*20+1+j, 7+4).value = f"c - {ws.cell(1+r, 1+4).value}"
                ws.cell(i*20+1+j, 7+5).value = f"d - {ws.cell(1+r, 1+5).value}"
                ws.cell(i*20+1+j, 7+6).value = chr(int(ws.cell(1+r, 1+6).value)+96)
        # for i, row in enumerate(ws.iter_rows()):
        break
        #     pass
    wb.save('done3.xlsx')

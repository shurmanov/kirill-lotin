import os
import urllib
from pathlib import Path

import telebot
from openpyxl import load_workbook
from io import BytesIO
from read_excel import translit_workbook
from transliterate import to_latin
from docx import Document

TOKEN = "1955174988:AAGmvPRDK0qnDf1UdEUMRgmIy9mCKXMKjOg"  # TODO: Read from env
bot = telebot.TeleBot(token=TOKEN)

PRIVATE_CHAT_ID = -560736223
# PRIVATE_CHAT_ID = -4085946215  # TODO: Read from env
CHANNEL_PUBLIC_USERNAME = "@exceldarslari"
# CHANNEL_PUBLIC_USERNAME = "@somelocaltestingchannel"
NOT_A_MEMBER_STR = f"Siz {CHANNEL_PUBLIC_USERNAME} kanalimizga a'zo emassiz. Iltimos, kanalimizga a'zo bo'ling va takror jo'nating."


def check_chat_member(user_id):
    try:
        chat_member = bot.get_chat_member(CHANNEL_PUBLIC_USERNAME, user_id)
        if not chat_member:
            return False
        if chat_member.status == 'left':
            return False
        return True
    except Exception as e:
        print(e)
        if 'user' in e.result_json['description']:
            return False
        bot.send_message(PRIVATE_CHAT_ID, f"😱😱😱{CHANNEL_PUBLIC_USERNAME} Kanalga botni admin qilib qo'shish kerak yoki user_id yo'q 😱😱😱")
        return True


def process_xlsx(file, f_name):
    wb = load_workbook(filename=BytesIO(file))
    wb = translit_workbook(wb)
    wb.save(f_name)
    doc = open(f_name, 'rb')
    return doc


def process_docx(file, f_name):
    doc = Document(docx=BytesIO(file))
    for para in doc.paragraphs:
        para.text = to_latin(para.text)

    for table in doc.tables:
        for row in table.rows:
            for cell in row.cells:
                cell.text = to_latin(cell.text)

    doc.save(f_name)
    doc = open(f_name, 'rb')
    return doc


extension_map = {
    '.xlsx': process_xlsx,
    '.docx': process_docx
}


@bot.message_handler(commands=['start'])
def send_welcome(message):
    username = message.from_user.username
    user_id = message.from_user.id
    is_member = check_chat_member(user_id)
    if not is_member:
        bot.send_message(message.chat.id, NOT_A_MEMBER_STR)
        return
    xabar = f'👋 Assalom alaykum, hurmatli {username}.\nKirill-Lotin botiga xush kelibsiz!'
    xabar += '\nYozing yoki Excel faylingizni yuboring.'
    bot.reply_to(message, xabar)
    # bot.send_message(PRIVATE_CHAT_ID, f" #user {message.from_user.id} {message.from_user.first_name} {message.from_user.username}")
    bot.send_message(PRIVATE_CHAT_ID, f" #chatid {message.chat.id}")


@bot.message_handler(content_types=['document', ])
def translit_document(message):
    user_id = message.from_user.id
    bot.send_message(PRIVATE_CHAT_ID, f"#chatid {message.chat.id}")
    is_member = check_chat_member(user_id)
    if not is_member:
        bot.send_message(message.chat.id, NOT_A_MEMBER_STR)
        return
    if PRIVATE_CHAT_ID:
        bot.forward_message(chat_id=PRIVATE_CHAT_ID, from_chat_id=message.chat.id, message_id=message.message_id)

    if message.document:
        extension = Path(message.document.file_name).suffix
        if extension in extension_map:
            tg_file = bot.get_file(message.document.file_id)
            f_name = f'media_files/{message.document.file_name[:-5]}_{message.document.file_unique_id}{extension}'
            url = 'https://api.telegram.org/file/bot{0}/{1}'.format(TOKEN, tg_file.file_path)
            file = urllib.request.urlopen(url).read()
            doc = extension_map[extension](file, f_name)
            msg = bot.send_document(message.chat.id, doc)
            os.remove(f_name)
            if PRIVATE_CHAT_ID:
                bot.forward_message(chat_id=PRIVATE_CHAT_ID, from_chat_id=message.chat.id, message_id=msg.message_id)
        else:
            bot.send_message(message.chat.id, f"Faqat excel ({', '.join([e for e in extension_map])}) kengaytmalik fayllar yoki matnqabul qilinadi")
            return


@bot.message_handler(content_types=['text', ])
def translit_text(message):
    user_id = message.from_user.id
    is_member = check_chat_member(user_id)
    if not is_member:
        bot.send_message(message.chat.id, NOT_A_MEMBER_STR)
        return

    if message.text:
        if PRIVATE_CHAT_ID:
            bot.forward_message(chat_id=PRIVATE_CHAT_ID, from_chat_id=message.chat.id, message_id=message.message_id)
        latin_text = to_latin(message.text)
        msg = bot.send_message(message.chat.id, latin_text)
        if PRIVATE_CHAT_ID:
            bot.forward_message(chat_id=PRIVATE_CHAT_ID, from_chat_id=message.chat.id, message_id=msg.message_id)


while True:
    try:
        bot.delete_webhook()
        bot.polling()
    except Exception as e:
        print(e)
        bot.send_message(PRIVATE_CHAT_ID, "😱😱😱Something went wrong😱😱😱")
